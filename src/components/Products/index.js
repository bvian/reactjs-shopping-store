import React from 'react';

import './Products.css';
import Product from '../Product';

class Products extends React.Component {
    render() {
        return (
            <div className='products'>
                <h2>All Products</h2>
                <div className='products-container'>
                    {
                        this.props.products.map((product) => {
                            return (
                                <Product key={product.id} product={product} />
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Products;