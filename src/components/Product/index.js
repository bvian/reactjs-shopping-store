import React from 'react';

import './Product.css';
import ProductDescription from '../layout/ProductDescription';

class Product extends React.Component {
    render() {
        return (
            <div className='product'>
                <img
                    className='productImage'
                    src={this.props.product.image}
                    alt=''
                />
                
                <ProductDescription title={this.props.product.title} rating={this.props.product.rating} />

                <div className='priceContainer'>
                    <h4>${this.props.product.price}</h4>
                    <a href='/'>
                        <i className='fal fa-shopping-cart cart'></i>
                    </a>
                </div>
            </div>
        );
    }
}

export default Product;