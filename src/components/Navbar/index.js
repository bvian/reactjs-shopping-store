import React from 'react';

import './Navbar.css';
import NavLeft from '../layout/NavLeft';
import NavMiddle from '../layout/NavMiddle';
import NavRight from '../layout/NavRight';
import MobileNavbar from '../layout/MobileNavbar';

class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            mobileNav: false
        };
    }

    showMobileNavbar = () => {
        this.setState({
            mobileNav: !this.state.mobileNav
        });
    }

    render() {
        return (
            <div id='header'>
                <NavLeft />
                <NavMiddle mobileNav={this.state.mobileNav} />
                <NavRight />
                <MobileNavbar showMobileNavbar={this.showMobileNavbar} />
            </div>
        );
    }
}

export default Navbar;