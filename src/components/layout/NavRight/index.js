import React from 'react';

import './NavRight.css';

class NavRight extends React.Component {
    render() {
        return (
            <div className="navRight">
                <ul className="navRightContainer">
                    <li><a href="/" id="signup1">Sign Up</a></li>
                    <li><a href="/" id="login1">Login</a></li>
                </ul>
            </div>
        );
    }
}

export default NavRight;