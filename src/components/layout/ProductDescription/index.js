import React from 'react';

import './ProductDescription.css';
import star from '../../../assets/images/star.png';
import user from '../../../assets/images/user.png';

class ProductDescription extends React.Component {
    render() {
        return (
            <div className='description'>
                <div className='productRating'>
                    <div className='rating'>
                        <span className='rate'>
                            <img
                                className='icon'
                                alt=''
                                src={star}
                            />
                            {this.props.rating.rate}
                        </span>
                        <span className='count'>
                            <img
                                className='icon'
                                alt=''
                                src={user}
                            />
                            {this.props.rating.count}
                        </span>
                    </div>
                </div>
                <h5>{this.props.title}</h5>
            </div>
        );
    }
}

export default ProductDescription;